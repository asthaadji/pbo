public class CellphoneMain {
    public static void main(String[] args){
        Cellphone cp = new Cellphone("Esia", "13939");
        cp.powerOn();
        cp.sleepmodeOn();
        cp.sleepmodeOff();
        cp.powerOff();

        // Dari sini semua function tidak bisa digunakan karena HP mati 
        cp.volumeDown();
        cp.sisaPulsa();

        // Setelah dihidupkan function bisa digunakan
        cp.powerOn();
        cp.volumeUp();
        cp.getVolume();
        cp.setVol(26);
        cp.getVolume();

        // Top Up pulsa diatas minimal 5000 & HP posisi hidup,
        // ketika posisi mati pulsa terkirim namun tidak bisa terlihat
        cp.topUpPulsa(15000);
        cp.sisaPulsa();

        // Menambahkan banyak contact dengan Array List
        Contact contact = new Contact("Riski", "13939");
        contact.tambahContact("Rama", "14045");
        contact.tambahContact("Dhani", "14022");
        contact.tambahContact("Putra", "12345");
        contact.tambahContact("Adi", "138855");
        contact.displayContacts();

        // Mencari Contact dengan Nama dan No
        contact.showContactByName("Irfan");
        contact.showContactByNoHP("13939");

    }
}
